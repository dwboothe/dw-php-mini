<?php
namespace Controllers;

use Illuminate\Database\Capsule\Manager as Capsule;
use Carbon\Carbon;
use Models;
use Slim\Http\Request;
//use Mail;

class PageController extends Controller
{
    function __construct($app, $eloquent){
        parent::__construct($app, $eloquent);
    }

    public function index(){
        $this->render('index.html.twig');    
    }
    public function home(){
        return $this->index();
    }
    public function about(){
        $this->render('about.html.twig');    
    }
    public function contact(){
        $vars = [];

        if($this->slimApp->request()->isPost()){
            //populate user fields
            $contact = new Models\Contact();
            $contact->name = $this->slimApp->request()->post('name');
            $contact->email = $this->slimApp->request()->post('email'); 
            $contact->message = $this->slimApp->request()->post('msg');
            $contact->save();
        }
        $this->render('contact.html.twig', $vars);   
    }
   
    protected function render($template, $data = null, $dir = 'pages/'){
        parent::render($dir.$template, $data);    
    }
}