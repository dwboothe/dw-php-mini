<?php
require_once dirname(__FILE__).'/../../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$config = require 'config.php';
$capsule->addConnection($config['db']);

// Set the event dispatcher used by Eloquent models... 
//$capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods
$capsule->setAsGlobal();

// Setup the Eloquent ORM
$capsule->bootEloquent();

