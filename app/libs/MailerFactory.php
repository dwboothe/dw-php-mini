<?php

class MailerFactory
{
    public static function build($carrier = "stmp")
    {
        if ($carrier == "stmp"){
            $transporter = \Swift_SmtpTransport::newInstance('smtp.server', 465, 'ssl')
            ->setUsername('user@email.com')
            ->setPassword('');
        } else if($carrier == "sendmail"){
            $transporter = \Swift_SendmailTransport::newInstance("/usr/sbin/sendmail -bs");
        } else {
            $transporter = \Swift_MailTransport::newInstance();
        }

        return \Swift_Mailer::newInstance($transporter);
    }
}