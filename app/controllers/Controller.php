<?php
namespace Controllers;

use Illuminate\Database\Capsule\Manager as Capsule;
use MailerFactory as MF;
use MessageFactory as MSGF;
use Models;

class Controller
{
    protected $slimApp;
    protected $mailer;
    protected $eloquent; //Capsule
    

    function __construct(\Slim\Slim $app, Capsule $eloquent){
        global $config;
        $this->slimApp = $app;
        $this->mailer = MF::build();
        $this->eloquent = $eloquent;

        //defualt emails
        $this->emailFrom = $config['email']['sender'];
        $this->errorEmailTo = $config['email']['error'];
        $this->emailTo = $config['email']['to'];

    }

    protected function render($template, $data = null){
        echo $this->slimApp->view->render($template, $data);    
    }

    public function notFound(){
        $this->slimApp->notFound();
    }

    protected function send($autoResponseEmail){
        try{
            //send mail
            $response = $this->mailer->send($autoResponseEmail);
        } catch (\Swift_TransportException $e){
            $error = $e->getMessage();
            $this->mailer = MF::build('mail');
            //send mail
            $response = $this->mailer->send($autoResponseEmail);
            $errorEmail = \Swift_Message::newInstance("Error sending email")
                            ->setTo($this->errorEmailTo)
                            ->setFrom($this->emailFrom)
                            ->setBody($error);
            $this->mailer->send($errorEmail);
        }   

        if(!$response){

            $errorEmail = \Swift_Message::newInstance("Error sending email: ". $autoResponseEmail->getSubject())
                        ->setTo($this->errorEmailTo)
                        ->setFrom($this->emailFrom)
                        ->setBody("The was an error sending an email.", 'text/plain');
            $this->mailer->send($errorEmail);
        }
        return $response;
    }

    protected function createMail($subject, $to, $from, $body, $bcc = null)
    {
        $newEmail = MSGF::build();
        $newEmail->setSubject($subject);
        $newEmail->setFrom($from);
        $newEmail->setTo($to);
        $newEmail->setBody($body, 'text/html');
        
        if(!empty($bcc)){
            $newEmail->setBcc($bcc);
        }

        return $newEmail;
    }

}