<?php
// set timezone for timestamps etc
date_default_timezone_set('UTC');
error_reporting(E_ALL);
ini_set('display_errors', 1);//turn off display error in production

define('DB_HOST', 'localhost');
define('DB_NAME', 'ait_mini');//your database name
define('DB_USER', 'ait_user');//your database user
define('DB_PASSWORD', 'ait_pass');//your databse password
define('DB_PORT', 3306);

define('MF_SENDER', 'dwboothe@gmail.com'); //address the email coming from
define('MF_ERROR', 'dwboothe@gmail.com'); //address for error emails
define('MF_TO', 'dwboothe@gmail.com'); //address to reply to

$config = array(
    'db' => array(    
           'host' => DB_HOST, 
           'username' => DB_USER, 
           'password' => DB_PASSWORD,
           //'port' => DB_PORT,
           'database' => DB_NAME,
           'driver' => 'mysql', 
           'collation' => 'utf8_unicode_ci',
           'charset' => 'utf8',
           'prefix' => '',
       ),
    'email' => array(    
           'sender' => MF_SENDER, 
           'error' => MF_ERROR, 
           'to' => MF_TO,
       ),
    );
return $config;



