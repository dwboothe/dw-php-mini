<?php

class MessageFactory
{
    public static function build()
    {
        $message = \Swift_Message::newInstance();

        $messageId = time().".".bin2hex(openssl_random_pseudo_bytes(5)).".someemail.com";
        $headers = $message->getHeaders();
        $headers->addIdHeader('Message-ID', $messageId);
        $headers->addTextHeader('MIME-Version', '1.0');
        $headers->addTextHeader('X-Mailer', 'PHP v' . phpversion());
        $headers->addParameterizedHeader('Content-type', 'text/html', ['charset' => 'utf-8']);

        return $message;
    }
}