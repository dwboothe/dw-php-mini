<?php
require_once dirname(__FILE__).'/../../vendor/autoload.php';

use Slim\Slim;
use Slim\Views\Twig as Twig;

$logWriter = new \Slim\LogWriter(fopen(dirname(__FILE__).'/../error_log', 'a+'));

//Create Slim app
$app = new Slim(
    [
    'debug' => true, 
    'view' => new Twig(), 
    'templates.path' => 'app/views', 
    'log.enabled' =>  true,
    'log.level' => \Slim\Log::EMERGENCY,
    'log.writer' => $logWriter
    ]
);

$view = $app->view;
$view->parserOptions = [
    'charset' => 'utf-8',
    /*'cache' =>'app/cache',*/
    'debug' => true
];
$view->parserExtensions = array(new \Slim\Views\TwigExtension());



