<?php
require_once 'vendor/autoload.php';

use Slim\Slim;
use Slim\Views\Twig as Twig;
use Illuminate\Database\Capsule\Manager as Capsule;
//use Illuminate\Events\Dispatcher;
//use Illuminate\Container\Container;

require 'app/config/config-capsule.php';
require 'app/config/config-slim.php';

/**
 * [callPage description]
 * @param  \Controllers\Controller $controller [description]
 * @param  String                 $page       [description]
 * @return [type]                             [description]
 */
function callPage(\Controllers\Controller $controller, $page, $args = array()){
    if(method_exists($controller,$page))
        $controller->$page($args);
    else 
        $controller->notFound();
}

$app->map('/(:page(/:args+))', function ($page = 'index', $args = array()) use($app, $capsule) {
    $controller = new \Controllers\PageController($app, $capsule);
    callPage($controller, $page, $args);
})->via('GET','POST')->setName('page');

$app->notFound(function () use ($app) {
    $app->render('404.html.twig');
});

//Run app
$app->run();

