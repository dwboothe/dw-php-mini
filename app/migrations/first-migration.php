<?php

//use Migration;

class FirstMirgation extends Migration
{
    public function up()
    {
        $this->schema->create('contact', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('message');
            $table->timestamps();
        });
    }
    public function down()
    {
        $this->schema->drop('contact');
    }
}
